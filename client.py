import grpc
import argparse
import google.protobuf.empty_pb2 as empty

from tts_pb2_grpc import AcousticStub
from tts_pb2 import InputText, Model


class AcousticClient(object):
    def __init__(self, remote='127.0.0.1:30001'):
        channel = grpc.insecure_channel(remote)
        self.stub = AcousticStub(channel)

    def txt2mel(self, speaker, text):
        return self.stub.Txt2Mel(InputText(speaker=speaker, text=text))

    def stream_txt2mel(self, speaker, text):
        return self.stub.StreamTxt2Mel(InputText(speaker=speaker, text=text))

    def get_mel_config(self):
        return self.stub.GetMelConfig(empty.Empty())

    def set_model(self, checkpoint_path):
        raise NotImplementedError

    def get_model(self):
        return self.stub.GetModel(empty.Empty())


if __name__ == '__main__':
    example_text = '교도통시는 중국-싱가포르간 외교 소식통을 이뇽해 시 주서기 붕미정상회다메 하비 내용 중 한반도 평화협쩡 체결 내용이 이쓸 경우 싱가포르를 방문할 계회기얻찌만, 평화협쩡 괄련 내용이 빠질 거스로 예상되자 싱가포르에 가지 아낟따고 설명핻씀니다.'
    parser = argparse.ArgumentParser(description='tacotron(DCA) TTS client')
    parser.add_argument('-r', '--remote', type=str, default='127.0.0.1:30001',
                        help="grpc: ip:port")
    parser.add_argument('-t', '--text', type=str, default=example_text,
                        help="input text. must be pre-processed by g2p.")
    parser.add_argument('-s', '--speaker', type=int, default=0,
                        help="speaker number")
    args = parser.parse_args()
    client = AcousticClient(args.remote)

    mel_config = client.get_mel_config()
    print(mel_config)

    mel = client.txt2mel(args.speaker, args.text)
    print(mel.data)

    for mel in client.stream_txt2mel(args.speaker, args.text):
        print(mel.data)

    model = client.get_model()
    print(model)
