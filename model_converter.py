import torch
import argparse

from tacotron import Tacotron


def _gen_hparams(config_paths):
    # generate hparams object for pl.LightningModule
    parser = argparse.ArgumentParser()
    parser.add_argument('--config')
    args = parser.parse_args(['--config', config_paths])
    return args


def convert_model(config_path, checkpoint_path, save_path):
    args = _gen_hparams(config_path)
    model = Tacotron(args)

    checkpoint = torch.load(checkpoint_path, map_location='cpu')
    model.load_state_dict(checkpoint['state_dict'])

    torch.save({
        'config_path': config_path,
        'hp': model.hp,
        'state_dict': model.state_dict(),
    }, save_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='dca model converter')
    parser.add_argument('-c', '--config', required=True, type=str,
                        help='yaml file for configuration')
    parser.add_argument('-i', '--input_model_path', required=True, type=str,
                        help='training model path.')
    parser.add_argument('-o', '--output_model_path', required=True, type=str,
                        help='inference model path.')

    args = parser.parse_args()

    convert_model(args.config, args.input_model_path, args.output_model_path)
