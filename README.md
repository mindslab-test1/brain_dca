# brain_dca

DCA (Dynamic Convolution Attention) 을 적용한 Tacotron2.

## Docker Registry에서 불러오기
`docker pull docker.maum.ai:443/brain_dca:<version>`

## Train
기존 tacotron2와 다르게 Phoneme이 input text로 들어와야 한다. 따라서 각 언어에 맞는 g2p를 활용해야 한다.

DCA_Tacotron2를 학습시키는 방법은 https://pms.maum.ai/confluence/x/71yqAQ 를 참고하면 된다.

또한, yaml 파일 작성 및 training option 설정 방법은 https://pms.maum.ai/confluence/x/JCeqAQ 에 쓰여 있다.

Baseline ckpt는 아래 구글 드라이브 링크에서 다운로드 가능하다.
- 한국어 (with brain_rbkog2p): https://drive.google.com/drive/folders/14iCQ99ZMAUra_kFbP3JMyhk21HleTXZN?usp=sharing
- English (with brain-g2p): https://drive.google.com/drive/folders/1n5cw7PetZ7siepAPTWS4dedEpvTsMoLf?usp=sharing

## Run
학습이 완료된 tacotron ckpt, vocoder (waveglow 혹은 hifi-gan) ckpt, config.yaml이 필요하다.

https://pms.maum.ai/confluence/x/pl2qAQ 에서 DCA 기반 TTS 서버를 띄우는 방법을 볼 수 있다.


## Author

담당자: 김강욱 연구원, 박승원 수석
